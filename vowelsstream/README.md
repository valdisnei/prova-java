
# Busca uma vogal em uma Stream

App que, dada uma stream, encontra o primeiro caracter Vogal, após uma consoante, 
onde a mesma é antecessora a uma vogal e que não se repita no resto da stream. Caso não encontre
o caracter vogal, uma mensagem.


## Pré-requisitos

- Java 1.8 


## Build

As seguintes tecnologias foram utilizadas:

- Java 8;
- JUnit 4;
- maven build

## Executando o sistema

Executar na raiz do projeto : *** maven clean package ***

**Resultado:** Executar : java -jar target/vowels-stream-1.0-SNAPSHOT.jar [stream desejada] .