package search.stream;

/**
 * Created by Valdisnei on 25/07/2017.
 */
public interface Stream {

    char getNext();

    boolean hasNext();
}
