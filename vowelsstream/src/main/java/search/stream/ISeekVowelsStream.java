package search.stream;

import java.util.Optional;

public interface ISeekVowelsStream {

    char NOT_FOUND = ' ';

    ISeekVowelsStream SeekVowelsStream = new SeekVowelsStream();

    ISeekVowelsStream seekChar(String _input);

    Optional<Character> build();
}
