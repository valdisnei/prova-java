package search.stream;


import search.exception.InvalidStreamStateException;

/**
 * Created by Valdisnei on 25/07/2017.
 */
class CharSequenceStream implements Stream {

    private final String source;

    private final char[] chars;

    private int currentIndex;

    private String texto;

    public CharSequenceStream(String source) {
        if (source == null) {
            throw new IllegalArgumentException("texto nao pode ser null");
        }
        this.source = source;
        this.chars = source.toCharArray();
    }

    public char getNext() {
        if (!hasNext()) {
            throw new InvalidStreamStateException();
        }
        return chars[currentIndex++];
    }

    public boolean hasNext() {
        return currentIndex < chars.length;
    }

    @Override
    public String toString() {
        return source;
    }
}
