package search.exception;

/**
 * Created by Valdisnei on 25/07/2017.
 */
public class InvalidStreamStateException extends RuntimeException {

    private static final long serialVersionUID = -6188074203275908046L;

    public InvalidStreamStateException() {
        super("Nao existem mais caracteres para serem processados");
    }
}
