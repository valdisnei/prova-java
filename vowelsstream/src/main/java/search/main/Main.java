package search.main;


import static search.stream.ISeekVowelsStream.NOT_FOUND;
import static search.stream.ISeekVowelsStream.SeekVowelsStream;

import java.util.function.Consumer;


/**
 * Created by Valdisnei on 25/07/2017.
 */
public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("\nPara executar: java -jar target/vowels-stream-1.0-SNAPSHOT.jar [stream desejada] ");
            System.exit(0);
        }

        SeekVowelsStream
                .seekChar(args[0])
                .build().
                ifPresent(new Consumer<Character>() {
                    @Override
                    public void accept(Character character) {
                        if (!String.valueOf(NOT_FOUND).equals(character))
                            System.out.println("\nResultado: " + character);
                        else
                            System.out.println("\nVogal nao localizada.");

                        System.exit(0);

                    }
                });

        System.out.println("\nVogal nao localizada.");


    }
}
