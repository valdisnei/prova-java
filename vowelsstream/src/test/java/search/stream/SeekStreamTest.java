package search.stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static search.stream.ISeekVowelsStream.SeekVowelsStream;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class SeekStreamTest {

    @Parameterized.Parameters(name = "input:{0} - output:{1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"aAbBABacafe", 'e'},
                {"AAAAAaaaa", ' '},
                {"afe", 'e'},
                {"cafe", 'e'},
                {"casa", ' '}
        });
    }

    private final String input;

    private final char expected;

    public SeekStreamTest(String input, char expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void findVowelOnStream() {
        char actual = SeekVowelsStream
                .seekChar(input)
                .build().get();
        assertThat(actual, is(equalTo(expected)));
    }
}
