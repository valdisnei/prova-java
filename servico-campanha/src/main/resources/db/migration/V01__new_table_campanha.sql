create table campanha
(
  id bigint auto_increment primary key,
  nome varchar(255) null,
  id_time_do_coracao bigint not null,
  data_inicial date not null,
  data_final date not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;