create table torcedor
(
  id bigint auto_increment primary key,
  nome varchar(255) null,
  email varchar(255) null,
  id_time_do_coracao bigint not null,
  data_nascimento date not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;