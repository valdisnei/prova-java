create table torcedor_campanha
(
  torcedor_id bigint not null,
  campanha_id bigint not null,
  PRIMARY KEY (`torcedor_id`,`campanha_id`),

  KEY FK_TORCEDORCAMPANHA_idx (torcedor_id),
  constraint FK_TORCEDORCAMPANHA_torcedor
  foreign key (torcedor_id) references torcedor (id) ON DELETE CASCADE ON UPDATE CASCADE,
  constraint FK_TORCEDORCAMPANHA_campanha
  foreign key (campanha_id) references campanha (id) ON DELETE CASCADE ON UPDATE CASCADE
);

