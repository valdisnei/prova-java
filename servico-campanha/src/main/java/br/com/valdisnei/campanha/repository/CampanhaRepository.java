package br.com.valdisnei.campanha.repository;

import br.com.valdisnei.campanha.model.Campanha;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Created by valdisnei on 7/22/17.
 */
public interface CampanhaRepository extends JpaRepository<Campanha,Long>{
    Optional<Campanha> findByDataFinal(LocalDate dataFinal);
}
