package br.com.valdisnei.campanha.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

/**
 * Created by valdisnei on 7/22/17.
 */
@AutoProperty
@Entity
@Table(name = "campanha")
public class Campanha implements Comparable<Campanha>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nome")
    private String nome;

    @NotNull
    @Column(name = "id_time_do_coracao")
    private Long idTimeCoracao;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "data_inicial")
    private LocalDate dataInicial;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "data_final")
    private LocalDate dataFinal;

    @JsonBackReference
    @ManyToMany(mappedBy = "campanhas")
    private Set<Torcedor> torcedores;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getIdTimeCoracao() {
        return idTimeCoracao;
    }

    public void setIdTimeCoracao(Long idTimeCoracao) {
        this.idTimeCoracao = idTimeCoracao;
    }

    public LocalDate getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(LocalDate dataInicial) {
        this.dataInicial = dataInicial;
    }

    public LocalDate getDataFinal() {return dataFinal;}

    public void setDataFinal(LocalDate dataFinal) {this.dataFinal = dataFinal;}

    public Set<Torcedor> getTorcedores() {return torcedores;}

    public void setTorcedores(Set<Torcedor> torcedores) {this.torcedores = torcedores;}

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }

    @Override
    public int compareTo(Campanha o) {
        if (this.id==null || o.id==null) return -1;
        return this.id.compareTo(o.id);
    }
}
