package br.com.valdisnei.campanha.resource;

import br.com.valdisnei.campanha.exceptionhandler.ServicoCampanhaExceptionHandler;
import br.com.valdisnei.campanha.model.Torcedor;
import br.com.valdisnei.campanha.service.CampanhaService;
import br.com.valdisnei.campanha.service.TorcedorService;
import br.com.valdisnei.campanha.service.exception.TorcedorEmailExistenteException;
import br.com.valdisnei.campanha.event.RecursoCriadoEvent;
import br.com.valdisnei.campanha.service.exception.TorcedorInexistenteOuInativaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by valdisnei on 7/23/17.
 */
@RestController
@RequestMapping("/api/torcedor")
public class TorcedorResource {

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private TorcedorService torcedorService;

    @Autowired
    private CampanhaService campanhaService;

    @Autowired
    private MessageSource messageSource;

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody Torcedor torcedor, HttpServletResponse response) {
        Optional<Torcedor> byEmail = Optional.ofNullable(torcedorService.findByEmail(torcedor.getEmail()));

        if (byEmail.isPresent() && byEmail.get().getCampanhas()!=null
                    && byEmail.get().getCampanhas().size()==0)
            return ResponseEntity.ok(campanhaService.list());

        if (byEmail.isPresent())
            throw new TorcedorEmailExistenteException();

        Torcedor torcedorCreated = torcedorService.create(torcedor);

        publisher.publishEvent(new RecursoCriadoEvent(this, response, torcedorCreated.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(torcedorCreated);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Torcedor> get(@PathVariable Long id,HttpServletResponse response) {
        Torcedor torcedor = torcedorService.get(id);
        publisher.publishEvent(new RecursoCriadoEvent(this, response, torcedor.getId()));

        return ResponseEntity.ok(torcedor);
    }

    @ExceptionHandler({TorcedorInexistenteOuInativaException.class })
    public ResponseEntity<Object> handlePessoaInexistenteOuInativaException(TorcedorInexistenteOuInativaException ex) {
        String mensagemUsuario = messageSource.getMessage("torcedor.inexistente-ou-inativa", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<ServicoCampanhaExceptionHandler.Erro> erros = Arrays.asList(new ServicoCampanhaExceptionHandler.Erro(mensagemUsuario, mensagemDesenvolvedor));
        return ResponseEntity.badRequest().body(erros);
    }

    @ExceptionHandler({TorcedorEmailExistenteException.class })
    public ResponseEntity<Object> handleTorcedorEmailExistenteException(TorcedorEmailExistenteException ex) {
        String mensagemUsuario = messageSource.getMessage("torcedor.existente", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<ServicoCampanhaExceptionHandler.Erro> erros = Arrays.asList(new ServicoCampanhaExceptionHandler.Erro(mensagemUsuario, mensagemDesenvolvedor));
        return ResponseEntity.badRequest().body(erros);
    }
}
