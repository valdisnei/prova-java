package br.com.valdisnei.campanha.repository;

import br.com.valdisnei.campanha.model.Torcedor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by valdisnei on 7/23/17.
 */
public interface TorcedorRepository extends JpaRepository<Torcedor,Long>{
    Torcedor findByEmail(String email);
}
