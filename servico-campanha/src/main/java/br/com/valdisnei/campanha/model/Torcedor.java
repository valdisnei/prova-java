package br.com.valdisnei.campanha.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.base.MoreObjects;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by valdisnei on 7/23/17.
 */
@AutoProperty
@Entity
@Table(name = "torcedor")
public class Torcedor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nome")
    private String nome;

    @NotNull
    @Column(name = "id_time_do_coracao")
    private Long idTimeCoracao;

    @NotNull
    @Column(name = "email")
    private String email;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "data_nascimento")
    private LocalDate dataNascimento;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "torcedor_campanha",
            joinColumns = @JoinColumn(name = "torcedor_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "campanha_id", referencedColumnName = "id"))
    private Set<Campanha> campanhas;

    public Torcedor(){
    }

    public Torcedor(Long id,String nome, Long idTimeCoracao, String email, LocalDate dataNascimento, Set<Campanha> campanhas) {
        this.id=id;
        this.nome = nome;
        this.idTimeCoracao = idTimeCoracao;
        this.email = email;
        this.dataNascimento = dataNascimento;
        this.campanhas = campanhas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getIdTimeCoracao() {
        return idTimeCoracao;
    }

    public void setIdTimeCoracao(Long idTimeCoracao) {
        this.idTimeCoracao = idTimeCoracao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }


    public Set<Campanha> getCampanhas() {
        return campanhas;
    }

    public void setCampanhas(Set<Campanha> campanhas) {
        this.campanhas = campanhas;
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }

    public static class BuilderTorcedor {
        private Torcedor torcedor;

        public BuilderTorcedor(Torcedor torcedor) {
            this.torcedor=MoreObjects.firstNonNull(torcedor,new Torcedor());
            this.torcedor.campanhas = MoreObjects
                    .firstNonNull(this.torcedor.campanhas,new TreeSet<>());
        }

        public BuilderTorcedor() {
            this.torcedor=new Torcedor();
            this.torcedor.campanhas = new TreeSet<>();
        }

        public BuilderTorcedor id(Long id){
            this.torcedor.id = id;
            return this;
        }

        public BuilderTorcedor nome(String nome){
            this.torcedor.nome = nome;
            return this;
        }

        public BuilderTorcedor idTimeCoracao(Long idTimeCoracao){
            this.torcedor.idTimeCoracao = idTimeCoracao;
            return this;
        }

        public BuilderTorcedor email(String email){
            this.torcedor.email = email;
            return this;
        }

        public BuilderTorcedor dataNascimento(LocalDate dataNascimento){
            this.torcedor.dataNascimento = dataNascimento;
            return this;
        }

        public BuilderTorcedor addCampanha(Campanha campanha){
            this.torcedor.campanhas.add(campanha);
            return this;
        }

        public Torcedor build(){
            return this.torcedor;
        }


    }
}