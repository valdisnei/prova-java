package br.com.valdisnei.campanha.service;

import br.com.valdisnei.campanha.model.Torcedor;
import br.com.valdisnei.campanha.repository.TorcedorRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by valdisnei on 7/22/17.
 */
@Service
public class TorcedorService {

    @Autowired
    private TorcedorRepository torcedorRepository;


    public Torcedor create(Torcedor torcedor){

        return torcedorRepository.save(torcedor);
    }

    public Torcedor atualizar(Long id, Torcedor _torcedor){

        Torcedor dbTorcedor = torcedorRepository.findOne(id);
        if (dbTorcedor == null) {
            throw new EmptyResultDataAccessException(1);
        }

        BeanUtils.copyProperties(_torcedor, dbTorcedor, "id");
        return torcedorRepository.save(dbTorcedor);
    }

    public void delete(Long id){
        Torcedor torcedor = torcedorRepository.findOne(id);
        if (torcedor == null) {
            throw new EmptyResultDataAccessException(1);
        }
        torcedorRepository.delete(id);
    }

    public List<Torcedor> list(){
        Optional<List<Torcedor>> all = Optional.ofNullable(torcedorRepository.findAll());


         return all.orElseThrow(()->new EmptyResultDataAccessException(1));
    }

    public Torcedor get(Long id) {
        Torcedor torcedor = torcedorRepository.findOne(id);
        if (torcedor == null) {
            throw new EmptyResultDataAccessException(1);
        }

        return torcedor;
    }

    public Torcedor findByEmail(String email) {
        return torcedorRepository.findByEmail(email);
    }
}
