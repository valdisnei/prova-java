package br.com.valdisnei.campanha.service;

import br.com.valdisnei.campanha.model.Campanha;
import br.com.valdisnei.campanha.repository.CampanhaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created by valdisnei on 7/22/17.
 */
@Service
public class CampanhaService {

    @Autowired
    private CampanhaRepository campanhaRepository;


    public Campanha create(Campanha campanha){

//        List<Campanha> all = campanhaRepository.findAll();
//
//        all.stream()
//                .filter(getPredicate(campanha))
//                .forEach(
//                        (c) -> c.getDataFinal().plusDays(1)
//                );

        return campanhaRepository.save(campanha);
    }

    private Predicate<Campanha> getPredicate(Campanha campanha) {
        Predicate<Campanha> predicate = new Predicate<Campanha>() {
            @Override
            public boolean test(Campanha _campanha) {
                return !Period.between(campanha.getDataInicial(),campanha.getDataFinal())
                              .isNegative();
            }
        };
        return predicate;
    }
    public Campanha atualizar(Long id, Campanha campanha){

        Campanha campanhaSalva = campanhaRepository.findOne(id);
        if (campanhaSalva == null) {
            throw new EmptyResultDataAccessException(1);
        }

        BeanUtils.copyProperties(campanha, campanhaSalva, "id");
        return campanhaRepository.save(campanhaSalva);
    }

    public void delete(Long id){
        Campanha campanhaSalva = campanhaRepository.findOne(id);
        if (campanhaSalva == null) {
            throw new EmptyResultDataAccessException(1);
        }
        campanhaRepository.delete(id);
    }

    public List<Campanha> list(){
        Optional<List<Campanha>> all = Optional.ofNullable(campanhaRepository.findAll());

        Predicate<Campanha> predicate = new Predicate<Campanha>() {
            @Override
            public boolean test(Campanha campanha) {
                return campanha.getDataInicial().isAfter(LocalDate.now())
                        || campanha.getDataInicial().equals(LocalDate.now());
            }
        };

         return all.orElseThrow(()->new EmptyResultDataAccessException(1));
    }

    public Campanha get(Long id) {
        Campanha campanha = campanhaRepository.findOne(id);
        if (campanha == null) {
            throw new EmptyResultDataAccessException(1);
        }

        return campanha;
    }
}
