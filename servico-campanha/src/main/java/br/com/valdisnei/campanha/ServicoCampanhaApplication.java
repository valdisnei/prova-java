package br.com.valdisnei.campanha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicoCampanhaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicoCampanhaApplication.class, args);
	}
}
