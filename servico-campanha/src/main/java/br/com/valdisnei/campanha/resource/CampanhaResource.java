package br.com.valdisnei.campanha.resource;

import br.com.valdisnei.campanha.model.Campanha;
import br.com.valdisnei.campanha.model.Torcedor;
import br.com.valdisnei.campanha.service.CampanhaService;
import br.com.valdisnei.campanha.service.TorcedorService;
import br.com.valdisnei.campanha.event.RecursoCriadoEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * Created by valdisnei on 7/22/17.
 */
@RestController
@RequestMapping("/api/campanha")
public class CampanhaResource {


    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private CampanhaService campanhaService;

    @Autowired
    private TorcedorService torcedorService;


    @PostMapping
    public ResponseEntity<Campanha> create(@Valid @RequestBody Campanha campanha, HttpServletResponse response) {
        Campanha campanhaSave = campanhaService.create(campanha);

        publisher.publishEvent(new RecursoCriadoEvent(this, response, campanhaSave.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(campanhaSave);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Campanha> get(@PathVariable Long id) {
        Campanha campanhaSave = campanhaService.get(id);

        return ResponseEntity.ok(campanhaSave);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Campanha> update(@PathVariable Long id,
                                           @Valid @RequestBody Campanha campanha) {
        Campanha campanhaSalva = campanhaService.atualizar(id, campanha);
        return ResponseEntity.ok(campanhaSalva);
    }

    @GetMapping
    public List<Campanha> list() {
        return campanhaService.list();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long id) {
        campanhaService.delete(id);
    }



    @PostMapping("/{id}")
    public ResponseEntity<Set<Campanha>> addCampanha(@PathVariable Long id,
                                                     @RequestBody Torcedor torcedor) {

        Torcedor byEmail = torcedorService.findByEmail(torcedor.getEmail());

        Torcedor build = new Torcedor.BuilderTorcedor(byEmail)
                .addCampanha(campanhaService.get(id))
                .build();

        return ResponseEntity.ok(torcedorService.atualizar(build.getId(),build).getCampanhas());
    }

}
