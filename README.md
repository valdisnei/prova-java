# Servico-campanha 

  Servico-campanha, fornece mecanismos (APIs) para
        INCLUIR, CONSULTAR, ATUALIZAR, DELETAR as campanhas.

## Pré-requisitos

- Java 8;
- JUnit 4;
- maven 3.x
- Mysql 5.x


## Stack
- Spring Boot 1.5.4
- Spring Data
- FlyWay 4.0
- Guava 22.0

## Compile
```mvn clean package```

## Executando o sistema

Executar na raiz do projeto : ***java -jar servico-campanha/target/servico-campanha-0.0.1-SNAPSHOT.jar***


## Servicos Campanha

Resources

***Cria campanha***
```
curl -X POST \
  http://localhost:8085/api/campanha \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
  "nome": "campeonato Paulista",
  "idTimeCoracao" : 2,
  "dataInicial" : "21-07-2017",
  "dataFinal" : "25-07-2017"
}'
```
***Consulta campanha***
```
curl -X GET \
  http://localhost:8085/api/campanha/1 \
  -H 'cache-control: no-cache' 
```

***Lista campanhas***
```
curl -X GET \
  http://localhost:8085/api/campanha \
  -H 'cache-control: no-cache' 
```
         
***Deleta campanha***
```
curl -X DELETE \
  http://localhost:8085/api/campanha/1 \
  -H 'cache-control: no-cache' 
```

***Atualiza campanha***
```
    curl -X PUT \
      http://localhost:8085/api/campanha/1 \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \
      -d '{
      "nome": "Libertadores",
      "idTimeCoracao" : 1,
      "dataInicial" : "21-07-2017",
      "dataFinal" : "23-07-2017"
    }'
```         
***Adiciona campanha para o Torcedor***
```
    curl -X POST \
      http://localhost:8085/api/campanha/1 \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \
      -d '{
      "email" : "joao.silva@gmail.com"
    }'
```

         
## Servicos Torcedor         

Resources

***Cadastra torcedor***
```
curl -X POST \
  http://localhost:8085/api/torcedor \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
  "nome": "Joao da Silva",
  "idTimeCoracao" : 1,
  "dataNascimento" : "18-11-2010",
  "email" : "joao.silva@gmail.com"
}'
```

***Consulta torcedor***
```
curl -X GET \
  http://localhost:8085/api/torcedor/1 \
  -H 'cache-control: no-cache' 
```



## Deadlock

Deadlock é quando 2 ou mais processos esperam um outro processo que pode estar também em espera.
Para tratar deadlocks a estratégia mais simples seria, evitar e prever, detectar e recuperar.


## ParallelStreams
Usamos ParallelStreams quando precisamos ganhar desempenho para filtrar grande volumes de dados.

Exemplo:
```java 
Stream<Empresa> streamEmpresa = listaEmpresa.parallelStream();
Integer somaEmpregados = streamEmpresa
                            .filter(p -> p.getNome().startsWith("A"))
                            .mapToInt(p -> p.getEmpregados).sum();
```






